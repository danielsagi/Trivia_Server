#ifndef DATABASE_H
#define DATABASE_H
#pragma once
#include <string>
#include <vector>
#include <vector>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "Question.h"

using namespace std;

class DataBase
{
public:
	DataBase();
	~DataBase();

	bool isUserAndPassMatch(string, string);
	bool isUserExist(string);
	bool addNewUser(string, string, string);
	vector<Question*> initQuestions(int);
	int insertNewGame();
	bool updateGameStatus(int GameId);
	bool addAnswerToPlayer(int gameid, string username, int qustionid, string answer, bool isCorrect, int answerTime);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string);

private:
	static int callbackQuestions(void*, int, char**, char**);
	static int callbackCount(void*, int, char**, char**);
	static int callbackForScore(void*, int, char**, char**);
	static int callbackPersonalStatus(void*, int, char**, char**);

};

#endif