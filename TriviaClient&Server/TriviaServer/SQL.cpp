#include "SQL.h"

vector<sqlTable> allTheTable;

SQL::SQL(string NameDataBase)
{
	// connection to the database
	_rc = sqlite3_open(NameDataBase.c_str(), &_db);
	if (_rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
	}
}

SQL::~SQL()
{
	if (allTheTable.size())
		clearVector();
}

string Message;
int CheckExsitTable(void* notUsed, int argc, char** argv, char** azCol)
{
	Message = argv[0];
	return 0;
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	return 0;
}


int callbackPrint(void* notUsed, int argc, char** argv, char** azCol)
{
	// casting to string* !
	string* RowIdName = new string[argc];
	for (int i = 0; i < argc; i++)
		RowIdName[i] = azCol[i];

	// casting to string* !
	string* RowData = new string[argc];
	for (int i = 0; i < argc; i++)
		RowData[i] = argv[i];

	// insert to the vector
	sqlTable row(RowIdName, argc);
	row.setRow(RowData);
	allTheTable.push_back(row);

	return 0;
}

bool SQL::createTable(string NameTable, string* IdName, string* Types, int LengthParams, bool FirstIsPrimaryKey)
{
	//create the command
	string CMD = "create table " + NameTable + " (";

	if (FirstIsPrimaryKey)	
		CMD += IdName[0] + " " + Types[0] + " primary key";
	else
		CMD += IdName[0] + " " + Types[0];

	for (int i = 1; i < LengthParams; i++)
		CMD += " , " + IdName[i] + " " + Types[i];
	
	CMD += ");";

	delete[] IdName;
	delete[] Types;

	//cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), callback, 0, &_zErrMsg);
	if (!ERRORS())
		return true;
	else
		return false;
}

bool SQL::existTable(string NameTable)
{
	//create the command
	string CMD = "select count(*) from sqlite_master where type='table' and name='" + NameTable + "';";


	//cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), CheckExsitTable, 0, &_zErrMsg);
	if (!ERRORS())
	{
		if (Message == "0")
			return false;
		else
			return true;
	}	
	else
		return false;
}

bool SQL::insertToTable(string NameTable, string* IdName, string* ParamsName, int LengthParams)
{
	//create the command
	string CMD = "insert into " + NameTable + " ( ";

	CMD += IdName[0];
	for (int i = 1; i < LengthParams; i++)
		CMD += " , " + IdName[i];
	CMD += " ) values ( ";


	if (isText(ParamsName[0]))
		CMD += "'" + ParamsName[0] + "'";
	else
		CMD += ParamsName[0];

	for (int i = 1; i < LengthParams; i++)
	{
		if (isText(ParamsName[i]))
			CMD += " , '" + ParamsName[i] + "'";
		else
			CMD += " , " + ParamsName[i];
	}
	CMD += " );";

	delete[] IdName;
	delete[] ParamsName;

	//cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), callback, 0, &_zErrMsg);
	if (!ERRORS())
		return true;
	else
		return false;
}

vector<sqlTable> SQL::getFromTable(string NameTable, string addCMD)
{
	string CMD = "select * from " + NameTable + " " + addCMD + ";";

	if (allTheTable.size())
		clearVector();

	cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), callbackPrint, 0, &_zErrMsg);
	ERRORS();

	return allTheTable;
}

bool SQL::updateRowInTable(string NameTable, string id,string updateCMD)
{
	//create the command
	string CMD = "update " + NameTable + " set " + updateCMD + " where " + id + ";";
	
	//cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), callback, 0, &_zErrMsg);
	if (!ERRORS())
		return true;
	else
		return false;
}

bool SQL::removeRowInTable(string NameTable, string id, bool removeAll)
{
	//create the command
	string CMD;
	if (removeAll)
		CMD = "delete from " + NameTable + ";";
	else
		CMD = "delete from " + NameTable + " where " + id + ";";

	//cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), callback, 0, &_zErrMsg);
	if (!ERRORS())
		return true;
	else
		return false;
}

bool SQL::deleteTable(string NameTable)
{
	//create the command
	string CMD = "drop table " + NameTable + ";";

	//cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), callback, 0, &_zErrMsg);
	if (!ERRORS())
		return true;
	else
		return false;
}

bool SQL::renameTable(string NameTable, string newName)
{
	//create the command
	string CMD = "alter table " + NameTable + " rename to " + newName + ";";

	//cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), callback, 0, &_zErrMsg);
	if (!ERRORS())
		return true;
	else
		return false;
}

bool SQL::addColumnToTable(string NameTable, string nameCol, string type)
{
	//create the command
	string CMD = "alter table " + NameTable + " add column " + nameCol + " " + type + ";";

	//cout << CMD << endl;
	_rc = sqlite3_exec(_db, CMD.c_str(), callback, 0, &_zErrMsg);
	if (!ERRORS())
		return true;
	else
		return false;
}

bool SQL::ERRORS()
{
	if (_rc != SQLITE_OK)
	{
		cout << "SQL error: " << _zErrMsg[0] << endl;
		sqlite3_free(&_zErrMsg[0]);
		return true;
	}
	else
		return false;
}

bool SQL::isText(string text)
{
	for (int i = 0; i < text.length(); i++)
	{
		if (!(text[i] <= '9' && text[i] >= '0'))
			return true;
	}
	return false;
}

void SQL::clearVector()
{
	for (int i = 0; i < allTheTable.size(); i++)
	{
		delete[] allTheTable[i].getIdName();
		delete[] allTheTable[i].getRow();
	}

	allTheTable.erase(allTheTable.begin(), allTheTable.end());
}
