#pragma once

#include <string>

using namespace std;

class DbUser
{
public:
	DbUser(string, string, string);
	~DbUser();

	string& getName();
	string& getPassword();
	string& getEmail();

private:
	string _name;
	string _password;
	string _email;
};

