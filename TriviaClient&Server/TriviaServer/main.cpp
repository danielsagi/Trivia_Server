#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "TriviaServer.h"

using namespace std;

int main()
{
	try 
	{
		WSAInitializer wsaInit;
		
		TriviaServer server;
		server.serve();
		server.~TriviaServer();
	}
	catch (exception &e)
	{
#ifdef DEBUG
		TRACE("%s\n", e.what())
#endif
	}

	getchar();
	return 0;
}