#ifndef ROOM_H
#define ROOM_H

#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

#include "User.h"
#include "Protocol.h"
#include "Helper.h"

using namespace std;

class User;

class Room
{
public:
	Room(int, User*, string, int, int, int);
	~Room();
	
	bool joinRoom(User*);
	void leaveRoom(User*);
	int closeRoom(User*);
	
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	User* getAdmin();
	string getName();

private:
	vector<User*> _users;
	User* _admin;
	unsigned int _maxUsers;
	int _questionTime;
	int _questionsNo;
	string _name;
	int _id;

	string getUsersAsString(vector<User*>, User*);
	void sendMessage(string);
	void sendMessage(User*, string);
};

#endif