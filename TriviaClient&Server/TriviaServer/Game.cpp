#include "Game.h"

Game::Game(const vector <User*> & players, int queNo, DataBase& db) : _questions_no(queNo), _players(players), _db(new DataBase(db)), _currQuestionIndex(0)
{
	// trying to insert a new game to database
	this->_ID = this->_db->insertNewGame();
	
	if (this->_ID != -1)
	{
		this->initQuestionsFromDB();
		
		// iterating over all room users, and assigning the new game to them
		for (vector<User*>::iterator player = this->_players.begin(); player != this->_players.end(); player++)
			(*player)->setGame(this);
	
		// initializing the results vector with usernames and 0 as their score
		for (vector<User*>::iterator player = this->_players.begin(); player != this->_players.end(); player++) {
			this->_results.insert(make_pair((*player)->getUsername(), 0));
		}
	}
	else
	{
		throw exception("ERROR: Game Failed To register");
	}
}

Game::~Game()
{
	// deleting questions from memory
	for (vector<Question*>::iterator question = this->_questions.begin(); question != this->_questions.end(); question++) {
		delete *question;
	}

	// clearing vectors
	this->_questions.clear();
	this->_players.clear();

#ifdef DEBUG
	TRACE("------------")
	TRACE("A Game Was Deleted From Memory\n")
	TRACE("------------\n")
#endif
}

bool Game::leaveGame(User* user)
{
	bool ret = false;
	
	// if there's only one user left, finishing the game first.
	/*if (this->_players.size() == 1)
		this->handleFinishGame();*/

	// finds user in list and removes him
	for (vector<User*>::iterator player = this->_players.begin(); player != this->_players.end(); player++) {
		if (*player == user) {
			this->_players.erase(player);
			
			// then operating next turn, and returning if game is still on
			if (this->handleNextTurn())
				ret = true;
			break;
		}
	}

	return ret;
}
void Game::sendFirstQuestion()
{
	this->sendQuestionToAllUsers();
}
void Game::handleFinishGame()
{
	this->_db->updateGameStatus(this->_ID);

	stringstream message;
	message << SERVER_END_GAME << this->_players.size();
	
	for (int i = 0; i < this->_players.size(); i++)
	{
		string username = this->_players[i]->getUsername();
		message << setfill('0') << setw(2) << username.length() << username; // username length, username
		message << setfill('0') << setw(2) << this->_results[username]; // results
	}

	// Sending the scores to all users, and clearing their game
	for (int i = 0; i < this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send(message.str());
			this->_players[i]->clearGame();
		}
		catch (exception &e)
		{
#ifdef DEBUG
			TRACE("%s\n", e.what())
#endif
		}
	}
}
bool Game::handleNextTurn()
{
	bool ret = false;

	if (this->_players.size() > 0)
	{
		if (this->_currentTurnAnswers >= this->_players.size()) { // if all the users had answered current question

			if (this->_currQuestionIndex == this->_questions_no - 1) // if current question is the last one
				this->handleFinishGame();
			else 
			{
				// if not, sending next one
				this->_currQuestionIndex++;
				this->sendQuestionToAllUsers();
				ret = true;
			}
		}
		else
		{
			ret = true;
		}
	}
	else
	{
		this->handleFinishGame();
	}

	return ret;
}
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	bool ret = false;
	stringstream message120;

	string username = user->getUsername();
	int questionID = this->_questions[this->_currQuestionIndex]->getId();
	string* answers = this->_questions[this->_currQuestionIndex]->getAnswers();
	
	// easy way to check for validity variations of the answer
	bool trueFlag = false, emptyAnswerFlag = false;
	
	message120 << SERVER_ANSWER_REPLY;
	if (answerNo == this->_questions[this->_currQuestionIndex]->getCorrectAnswerIndex() + 1)
	{
		trueFlag = true;
		this->_results[username]++;
		message120 << 1;
	}
	else 
	{
		if (answerNo == 5) 
			emptyAnswerFlag = true;
		message120 << 0;
	}

	// constructing a the query parameters using flags.
	this->_db->addAnswerToPlayer(this->_ID, username, questionID, (emptyAnswerFlag ? "":answers[answerNo]), trueFlag, time);

	this->_currentTurnAnswers++;
	user->send(message120.str());

	if (this->handleNextTurn())
		ret = true;

	return ret;
}
int Game::getID()
{
	return this->_ID;
}

// PRIVATE FUNCTIONS
bool Game::insertGameToDB()
{
	return true;
}
void Game::initQuestionsFromDB()
{
	this->_questions = this->_db->initQuestions(this->_questions_no);
}
void Game::sendQuestionToAllUsers()
{
	stringstream message108;
	this->_currentTurnAnswers = 0;

	
	// getting current question
	string question = this->_questions[this->_currQuestionIndex]->getQuestion();
	string* answers = this->_questions[this->_currQuestionIndex]->getAnswers();

	
	// building message
	message108 << SERVER_START_GAME << setfill('0') << setw(3) << question.length() << question;
	for (int i = 0; i < 4; i++)
		message108 << setfill('0') << setw(3) << answers[i].length() << answers[i];

	// sending message to all users
	for (unsigned int i = 0; i < this->_players.size(); i++) {
		try {
			this->_players[i]->send(message108.str());
		}
		catch (exception &e) {
#ifdef DEBUG
			TRACE("%s\n", e.what())
#endif
		}
	}	
}