#pragma once
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class sqlTable
{
public:
	sqlTable(string* IdName,int len);
	~sqlTable();

	void setRow(string* row);
	string operator[](string IdName);
	string operator[](int id);

	int getLenRow();
	string* getIdName();
	string* getRow();

private:
	string* _IdName;
	int _len;

	string* _Row;
};

