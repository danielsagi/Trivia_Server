#ifndef USER_H
#define USER_H

#include <WinSock2.h>
#include <Windows.h>

#include <string>

#include "Helper.h"
#include "Room.h"
#include "Game.h"


class Game;
class Room;

using namespace std;

class User
{
public:
	User(string, SOCKET);
	~User();

	string getUsername();
	void send(string message);
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* game);
	void clearRoom();
	void clearGame();
	bool createRoom(int id,string name, int maxUsers, int questionTime, int questionNo);
	bool joinRoom(Room* room);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();

private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};

#endif