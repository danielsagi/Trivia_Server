#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4) : _id(id), _question(question)
{
	array<string, 4> shuffAnswers = {correctAnswer, answer2, answer3, answer4};

	// shuffling answers
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	shuffle(shuffAnswers.begin(), shuffAnswers.end(), std::default_random_engine(seed));

	// finally converting the template array to our primitive one 
	for (int i = 0; i < 4; i++) {
		this->_answers[i] = shuffAnswers[i];

		// inserting the correct answer's index to its property
		if (this->_answers[i] == correctAnswer)
			this->_correctAnswerIndex = i;
	}
		
}

Question::~Question()
{
}

string Question::getQuestion() { return this->_question; }
string* Question::getAnswers() { return this->_answers; }
int Question::getCorrectAnswerIndex() { return this->_correctAnswerIndex; }
int Question::getId() { return this->_id; }
