#pragma once
#include <iostream>
#include "DataBase.h"
#include "sqlite3.h"
#include <cstdio>
#include <cstdlib>
#include <vector>
#include "Question.h"
#include <ctime>   
#include <random>
#include <string> 
#include <algorithm>
using namespace std;

string tempData;//global string to getout the result from sql (wite a callbackfunc)
sqlite3* db;
bool noErrors(int  rc, char **zErrMsg)//this func chek if there is any error , if no she return true if yes she make system pause and return false
{
	if (rc != SQLITE_OK)//if rc(the int that return from sqlite3_exec func) is diffrent from the define in sqlite3.h that sey is good , this is error
	{
#ifdef DEBUG
		TRACE("%s", *zErrMsg)
#endif
		sqlite3_free(zErrMsg[0]);//free the varb 
		return false;
	}
	else//if no return true (no errors)
	{
		return true;
	}
}
int callbacki(void* notUsed, int argc, char** argv, char** azCol)//take the argv[0] and put him in a string (global verb) , return 0 
{
	tempData = argv[0];//if somthing , so good .if nothing ,so a will be = ""
	return 0;
}


/* Returning an int representation of the first column data given */
int DataBase::callbackCount(void* data, int argc, char** argv, char** azColName)
{
	int* ret = (int*)data;
	*ret = stoi(argv[0]);

	// quick check to see if found a value
	return *ret ? 0: -1;
}

/* take the argv[0] and put him in a string (global verb) , return 0 */
int DataBase::callbackForScore(void* data, int argc, char** argv, char** azCol)
{
	vector<string>* ret = static_cast<vector<string>*>(data);

	if (argv[0] != NULL || argv[0] == nullptr && argv[1] != NULL || argv[1] == nullptr) {
		ret->push_back(argv[1]);
		ret->push_back(argv[0]);
	}

	return 0;
}

/* Pushing to given vector all the found questions (creating them) */
int DataBase::callbackQuestions(void* data, int argc, char** argv, char** azColName)
{
	// casting the void* to a vector
	vector<Question*>* vektor = static_cast<vector<Question*>*>(data);

	// pushing to given vector the found question, with all its values
	if (argv[0])
		vektor->push_back(new Question(atoi(argv[0]), argv[1], argv[2], argv[3], argv[4], argv[5]));

	return 0;
}

/* At each call, counting the given row's detailes and storing in given Map */
int DataBase::callbackPersonalStatus(void* data, int argc, char** argv, char**azColName)
{
	// casting the void* to a map
	vector<string>* retVector = static_cast<vector<string>*>(data);

	// building vector with data recieved from database
	for (int i = 0; i < argc; i++)
		if (argv[i])
			retVector->push_back(argv[i]);

	// if user has no games and answers, returns as an error
	return (retVector->size() >= 2 && (*retVector)[0] != "0" && (*retVector)[1] != "0" ? 0 : -1);
}


DataBase::DataBase()
{
	int rc;
	rc = sqlite3_open("triviaDB.db", &db);
	if (rc)
	{
		throw exception("DB not open");
	}
}
DataBase::~DataBase()
{
	sqlite3_close(db);
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	tempData = "-999";
	int rc;//to the sqlite3_exec
	char* zErrMsg;
	bool ret = false;

	rc = sqlite3_exec(db, ("select password from t_users where username = '" + username + "';").c_str(), callbacki, 0, &zErrMsg);
		
	// if there weren't any errors, and the user has been found 
	if (noErrors(rc, &zErrMsg) && tempData == password)
		ret = true;

	return ret;
}
bool DataBase::isUserExist(string name)
{
	tempData = "";//to reset him - its global
	int rc;//to the sqlite3_exec
	char* zErrMsg;
	bool ret = false;

	rc = sqlite3_exec(db, ("select username from t_users where username = '" + name + "';").c_str(), callbacki, 0, &zErrMsg);
		
	// if no errors were found and we found the user.
	if (noErrors(rc, &zErrMsg) && tempData == name)
		ret = true;

	return ret;
}
bool DataBase::addNewUser(string username, string password, string email)
{
	tempData = "";
	int rc;
	char* zErrMsg;
	bool ret = false;

	rc = sqlite3_exec(db, ("insert into t_users(username , password , email) values('" + username + "','" + password + "','" + email + "');").c_str(), NULL, 0, &zErrMsg); // if username match any username its return the username (to a ( a its global)) and chek if user name = a if yes so its good !
	
	if (noErrors(rc, &zErrMsg))
		ret = true;
	
	return ret;
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	stringstream query;
	vector<Question*> ret; 
	int numOfQuestions;
	tempData = "";//to reset him - its global
	int rc;//to the sqlite3_exec
	char* zErrMsg;

	// getting the number of questions there are currently in DB
	rc = sqlite3_exec(db,("select COUNT(*) from t_questions"), callbacki, 0, &zErrMsg);
	if (noErrors(rc, &zErrMsg))
	{
		if (tempData != "")//if this = "" (its mean if a not = to nothing) - so good no error
			numOfQuestions = stoi(tempData);
		else if (tempData == "0")
			throw exception("no questions");
	}

	
	if (numOfQuestions >= questionsNo)
	{
		vector<int> questionIds;

		// generating random indexes according to number of questions in DB and the num of Questions requested
		// Creating a shuffled vector of all indices up to the num of Questions in DB 
		for (int i = 1; i <= numOfQuestions; i++) {
			questionIds.push_back(i);
		}
		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
		shuffle(questionIds.begin(), questionIds.end(), std::default_random_engine(seed));

		// we use only the number of random shuffled indices we need.
		for (int j = 0; j < questionsNo && noErrors(rc, &zErrMsg); j++)
		{
			// Now for each random index, we insert to the ret vector, its (id) corresponding question
			// The callback will take care of the creation and insertation of the found question
			query << "select question_id, question, correct_ans, ans2, ans3, ans4 from t_questions where question_id = " << questionIds[j] << ";";
			rc = sqlite3_exec(db, query.str().c_str(), callbackQuestions, &ret, &zErrMsg);
			query.str("");
		}
	}

	return ret;
}
int DataBase::insertNewGame()
{
	char* zErrMsg;
	stringstream query;
	int retID = -1;

	query << "insert into t_games(status, start_time) values(" << 0 << ", datetime('now', 'localtime') );";
	
	int rc = sqlite3_exec(db, query.str().c_str(), NULL, NULL, &zErrMsg);
	
	if (noErrors(rc, &zErrMsg))
	{
		// if succeeded, gets the id of the game ( last inserted == max(PK) ) 
		query.str("");
		query << "SELECT max(game_id) FROM t_games;";
		rc = sqlite3_exec(db, query.str().c_str(), callbackCount, &retID, &zErrMsg);
		
		noErrors(rc, &zErrMsg);
	}

	return retID;
}
bool DataBase::updateGameStatus(int GameId)
{
	char* zErrMsg;
	stringstream query;
	bool ret = false;

	query << "update t_games set status=1 where game_id=" << GameId << ";";
	int rc = sqlite3_exec(db, query.str().c_str(), NULL, NULL, &zErrMsg);
	
	if (noErrors(rc, &zErrMsg))
		ret = true;

	return ret;
}
bool DataBase::addAnswerToPlayer(int gameid, string username, int qustionid, string answer, bool isCorrect, int answerTime)
{
	char* zErrMsg;
	stringstream query;
	bool ret = false;

	// building the insert query
	query << "insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) ";
	query << "values(" << gameid << ", '" << username << "', " << qustionid << ", '" << answer << "', " << (isCorrect ? 1 : 0) << ", " << answerTime << ");";

	int rc = sqlite3_exec(db, query.str().c_str(), NULL, NULL, &zErrMsg);
	if (noErrors(rc, &zErrMsg))
		ret = true;
	return ret;
}

vector<string> DataBase::getBestScores()
{
	tempData = "";//to reset him - its global
	int rc;//to the sqlite3_exec
	char* zErrMsg;

	vector<string> ret;

	rc = sqlite3_exec(db, ("select count(*), username from t_players_answers where is_correct = 1 group by (username) order by(count(*)) DESC LIMIT 3;"), this->callbackForScore, &ret, &zErrMsg);

	ret;

	if (noErrors(rc, &zErrMsg) == false)
	{
		throw exception("error");
	}

	return ret;
}
vector<string> DataBase::getPersonalStatus(string username)
{
	int rc, count;
	char* zErrMsg;
	stringstream query, helpParser;
	vector<string> retVector;
	vector<string> finishedVec;
	
	// Building query, for the entire message
	query << "SELECT COUNT(DISTINCT game_id), SUM(is_correct = 1), SUM(is_correct = 0), AVG(answer_time)";
	query << "FROM t_players_answers where username = '" << username << "';";

	rc = sqlite3_exec(db, query.str().c_str(), this->callbackPersonalStatus, &retVector, &zErrMsg);
	if (noErrors(rc, &zErrMsg))
	{
		// building the formatted data from the callback, with padding Zeros

		// RetVector[0] == Game Count			(4)       
		helpParser << setfill('0') << setw(4) << retVector[0];
		finishedVec.push_back(helpParser.str());
		helpParser.str("");
	
		// RetVector[1] == Right Answers		(6)
		helpParser << setfill('0') << setw(6) << retVector[1];
		finishedVec.push_back(helpParser.str());
		helpParser.str("");

		// RetVector[2] == Wrong Answers		(6)
		helpParser << setfill('0') << setw(6) << retVector[2];
		finishedVec.push_back(helpParser.str());
		helpParser.str("");

		// RetVector[3] == Avg Time For Answer  (4)
		string fullNumber(retVector[3]);
		string dec = fullNumber.substr(0, fullNumber.find('.'));   // getting the number before the dot
		string fract = fullNumber.substr(fullNumber.find('.') + 1, 2); // getting the number after the dot

		helpParser << setfill('0') << setw(2) << dec << setfill('0') << setw(2) << fract;
		finishedVec.push_back(helpParser.str());
	}
	else
	{
		// inserting null values, in case of an error.
		for (int i = 0; i < 4; i++)
			finishedVec.push_back("00000");
	}

	return finishedVec;
}
