#include "DbUser.h"

DbUser::DbUser(string name, string password, string email)
{
	this->_name = name;
	this->_password = password;
	this->_email = email;
}

DbUser::~DbUser() {}

string& DbUser::getName() {	
	return this->_name;
}		

string& DbUser::getPassword() {		
	return this->_password;
}		

string& DbUser::getEmail() {		
	return this->_email;
}		
