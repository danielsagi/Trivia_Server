#ifndef Received_MESSAGE
#define Received_MEESAGE

#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <string>
#include <vector>

class User;

using namespace std;

class ReceivedMessage {
public:
	ReceivedMessage(SOCKET sock, int code);
	ReceivedMessage(SOCKET sock, int code, vector<string> values);
	~ReceivedMessage();

	SOCKET getSock();
	User* getUser();
	void setUser(User* set);
	int getMessageCode();
	vector<string>& getValues();

private:
	int _MessageCode;
	vector<string> _values;
	SOCKET _sock;
	User* _user;
};

#endif

