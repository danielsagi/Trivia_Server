#include "ReceivedMessage.h"

ReceivedMessage::ReceivedMessage(SOCKET s, int code){

	_sock = s;
	_MessageCode = code;
	_user = nullptr;
}
ReceivedMessage::ReceivedMessage(SOCKET s, int code, vector<string> values)
{
	_sock = s; 
	_MessageCode = code;
	_values = values;
	_user = nullptr;
}
ReceivedMessage::~ReceivedMessage() 
{
}

SOCKET ReceivedMessage::getSock()
{
	return _sock;
}

User* ReceivedMessage::getUser()
{
	return _user;
}

void ReceivedMessage::setUser(User* set)
{
	_user = set;
}

int ReceivedMessage::getMessageCode()
{
	return _MessageCode;
}

vector<string>& ReceivedMessage::getValues()
{
	return _values;
}