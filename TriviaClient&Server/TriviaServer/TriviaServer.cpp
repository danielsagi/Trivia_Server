#include "TriviaServer.h"
#include "Helper.h"

int TriviaServer::_roomIdSequence = 0;

/* c'tor and d'tor */
TriviaServer::TriviaServer()
{
	// configuring socket to TCP at startup
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (this->_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

TriviaServer::~TriviaServer()
{
	try
	{
		// freeing the received messages memory, if needed.
		for (unsigned int i = 0; i < this->_queRcvMessages.size(); i++)
		{
			delete _queRcvMessages.front();
			_queRcvMessages.pop();
		}

		// freeing connected users
		for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
		{
			// adding an exit message to queue for each of the connected users, for the handler function to disconnect them
			this->addReceivedMessage(this->buildRecieveMessage(it->first, CLIENT_EXIT));
			
			delete it->second;
		}
		::closesocket(this->_socket);
		
	}
	catch (...) {}
}


//--------------- MAIN MT SERVER FUNCTIONS ---------------//

/* main funciton to take care of all the procedure to open threads and configure socket.*/
void TriviaServer::serve()
{
	try {
		this->bindAndListen();

		// creating another thread to wait on incoming messages and handle them
		thread messageHandler(&TriviaServer::handleReceivedMessages, this);
		messageHandler.detach();
	
		while (true)
		{
			cout << "Waiting for client connection request" << endl;

			//the main thread is only accepting clients 			
			this->accept();
		}
	}
	catch (exception &e) {
#ifdef DEBUG
		TRACE("%s\n", e.what())
#endif
	}
}

/* Function binds socket and start the listening */
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	int port = 0;
	ifstream portFile("portConfig.txt");
	string line;

	if (portFile.is_open())
	{
		getline(portFile, line);
		port = atoi(line.c_str());
		portFile.close();
	}
	else
	{
		port = PORT;
	}

	sa.sin_port = htons(port);		// port that server will listen for
	sa.sin_family = AF_INET;		// must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;// when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;
}

/* Function is generally handling the client's requests */
void TriviaServer::clientHandler(SOCKET client)
{
	int messageCode = 1;
	
	// if message is valid and is not a disconnecting type message
	// keeps building and adding messages to queue
	while (messageCode != 0 && messageCode != CLIENT_EXIT)
	{
		// gets new message
		try {
			messageCode = Helper::getMessageTypeCode(client);
		}
		catch (...) {
			messageCode = CLIENT_EXIT;
		}

		ReceivedMessage* currentMessage = this->buildRecieveMessage(client, messageCode);
		addReceivedMessage(currentMessage);

		// notifies handle messages thread, a message has been added
		this->_rcvMessagesCond.notify_one();
	}
}

/* Function adds a given message to queue. while maintaining thread safe rules */
void TriviaServer::addReceivedMessage(ReceivedMessage* message)
{
	// locking before any adjustments to the shared queue
	lock_guard<mutex> lock(this->_mtxReceivedMessages);
	
	this->_queRcvMessages.push(message);

	// lock is unlocked when the function returns
}

/* Function accepts client and opening for it a seperate thread*/
void TriviaServer::accept()
{
	// this waits until a client is requesting to connect 
	// and creates a specific socket from server to this client
	SOCKET client_socket = ::accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;

	// the function that handle the conversation with the client
	thread clientThread(&TriviaServer::clientHandler, this, client_socket);
	clientThread.detach();
}

/* Function builds and returns a RecievedMessage message by inseting the given parameters into it*/
ReceivedMessage* TriviaServer::buildRecieveMessage(SOCKET client, int messageCode)
{
	ReceivedMessage* message;	
	vector<string> values;

	int tempLength = 0;

	// adding to vector the necessary values for each message
	switch (messageCode)
	{
		case CLIENT_SIGN_IN:
			tempLength = Helper::getIntPartFromSocket(client, 2);
			values.push_back(Helper::getStringPartFromSocket(client, tempLength)); // Username
			tempLength = Helper::getIntPartFromSocket(client, 2);
			values.push_back(Helper::getStringPartFromSocket(client, tempLength)); // Password
			break;

		case CLIENT_SIGN_UP:
			tempLength = Helper::getIntPartFromSocket(client, 2);
			values.push_back(Helper::getStringPartFromSocket(client, tempLength)); // Username
			tempLength = Helper::getIntPartFromSocket(client, 2);
			values.push_back(Helper::getStringPartFromSocket(client, tempLength)); // Password
			tempLength = Helper::getIntPartFromSocket(client, 2);
			values.push_back(Helper::getStringPartFromSocket(client, tempLength)); // Email
			break;

		case CLIENT_ROOMS_USERS:
			values.push_back(Helper::getStringPartFromSocket(client, 4)); // Room ID
			break;

		case CLIENT_JOIN_ROOM:
			values.push_back(Helper::getStringPartFromSocket(client, 4)); // Room ID
			break;

		case CLIENT_CREATE_ROOM:
			tempLength = Helper::getIntPartFromSocket(client, 2);
			values.push_back(Helper::getStringPartFromSocket(client, tempLength)); // Room Name
			values.push_back(Helper::getStringPartFromSocket(client, 1)); // Number of Players 1-9
			values.push_back(Helper::getStringPartFromSocket(client, 2)); // Number of questions
			values.push_back(Helper::getStringPartFromSocket(client, 2)); // Number of seconds to answer a question
			break;

		case CLIENT_ANSWER:
			values.push_back(Helper::getStringPartFromSocket(client, 1)); // Answer Number
			values.push_back(Helper::getStringPartFromSocket(client, 2)); // Time in seconds
			break;

		// empty message
		default:
			break;
	}
	
	if (values.size() == 0)
		message = new ReceivedMessage(client, messageCode);
	else
		message = new ReceivedMessage(client, messageCode, values);

	return message;
}


/* FUNCTION HANDLES MESSAGES FROM QUEUE */ 
void TriviaServer::handleReceivedMessages()
{
	bool exit = false;
	ReceivedMessage* message = NULL;

	try
	{
		while (true)
		{

			//// this will unlock the mutex and will put the thread to sleep until gets notified.
			//// once notified, the mutex will be *LOCKED* and the thread will wake up
			//// for the case of spurious wake, we're passing a lambda function which makes sure that the queue is empty, in the case of a wake
			unique_lock<mutex> messageLock(this->_mtxReceivedMessages);
			this->_rcvMessagesCond.wait(messageLock, [&]() {return !this->_queRcvMessages.empty(); });

			message = this->_queRcvMessages.front();
			this->_queRcvMessages.pop();

			// saving the message and unlocking 
			messageLock.unlock();

			// In te case that there is one getting the user associated with the socket that is attached to the current message.
			// and setting it into the message object.
			message->setUser(getUserBySocket(message->getSock()));

			// calling the appropriate function according to message code.
			switch (message->getMessageCode())
			{
			case CLIENT_SIGN_IN:
				this->handleSignin(message);
				break;

			case CLIENT_SIGN_UP:
				this->handleSignup(message);
				break;

			case CLIENT_SIGN_OUT:
				this->handleSignOut(message);
				break;

			case CLIENT_CREATE_ROOM:
				this->handleCreateRoom(message);
				break;

			case CLIENT_JOIN_ROOM:
				this->handleJoinRoom(message);
				break;

			case CLIENT_LEAVE_ROOM:
				this->handleLeaveRoom(message);
				break;

			case CLIENT_CLOSE_ROOM:
				this->handleCloseRoom(message);
				break;

			case CLIENT_ROOMS_LIST:
				this->handleGetRooms(message);
				break;

			case CLIENT_ROOMS_USERS:
				this->handleGetUsersInRoom(message);
				break;

			case CLIENT_BEGIN_GAME:
				this->handleStartGame(message);
				break;

			case CLIENT_LEAVE_GAME:
				this->handleLeaveGame(message);
				break;

			case CLIENT_ANSWER:
				this->handlePlayerAnswer(message);
				break;

			case CLIENT_PERSONAL_STATUS:
				this->handleGetPersonalStatus(message);
				break;
			
			case CLIENT_BEST_SCORES:
				this->handleGetBestScores(message);
				break;
			
			// Exit Message or Other
			default:
				this->safeDeleteUser(message);
				break;
			}
		}

		delete message;
	}
	catch (exception &e)
	{
#ifdef DEBUG
		TRACE("%s\n", e.what())
#endif
		this->safeDeleteUser(message);
		delete message;
	}
}




//------------- OTHER FUNCTIONS -----------------//

// USER HANDLERS
User* TriviaServer::handleSignin(ReceivedMessage* msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	
	stringstream message;
	User* retUser = nullptr;

	if (this->_db.isUserAndPassMatch(username, password))
	{
		retUser = this->getUserByName(username);
		if (retUser != nullptr)
		{
			// user is connected
			retUser = nullptr;
			message << SERVER_SIGN_IN << EXCEPT_CODE;
		}
		else
		{
			//// getting user by his socket, so knowing
			//retUser = this->getUserBySocket(msg->getSock());
			//msg->setUser(retUser);

			//// signing out each user, for case of already connected
			//this->handleSignOut(msg);
			
			// user is not connected, Its okay to Sign In
			retUser = new User(username, msg->getSock());
			this->_connectedUsers.insert(make_pair(msg->getSock(), retUser));

			message << SERVER_SIGN_IN << SUCCESS_CODE;
		}
	}
	else
	{
		// user does not exists in db 
		message << SERVER_SIGN_IN << FAILED_CODE;
	}

	Helper::sendData(msg->getSock(), message.str());

	return retUser;
}
bool TriviaServer::handleSignup(ReceivedMessage* msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	string email = msg->getValues()[2];

	stringstream message;

	message << SERVER_SIGN_UP;

	try
	{
		if (Validator::isPasswordValid(password))
		{
			if (Validator::isUsernameValid(username))
			{
				if (!this->_db.isUserExist(username))
				{
					// The Golden Condition. Username And Password Are Valid, Also User Doesn't Exists In DB
					if (this->_db.addNewUser(username, password, email))
						message << 0;
					else
						message << 1;
				}
				else
					message << 2;
			}
			else
				message << 3;
		}
		else
			message << 1;
	}
	catch (...)
	{
		message.str("");
		message << 4;
	}

	Helper::sendData(msg->getSock(), message.str());

	return true;
}
void TriviaServer::handleSignOut(ReceivedMessage* msg)
{
	User* user = msg->getUser();
	if (user != nullptr)
	{
		// takes care of leaving and deleting the room and the game
		this->handleLeaveGame(msg);
		this->handleCloseRoom(msg); // ?? Orientation
		this->handleLeaveRoom(msg);

		// before disconnecting
		this->_connectedUsers.erase(this->_connectedUsers.find(user->getSocket()));
		// ?? add support for searching multiple users for one socket.
	}
}

// GAME HANDERS
void TriviaServer::handleStartGame(ReceivedMessage* msg)
{
	User* user = msg->getUser();
	Room* userRoom = user->getRoom();
	vector<User*> usersInRoom = userRoom->getUsers();
	stringstream message;

	try 
	{
		// removing room from waiting rooms list, and closing it
		this->_roomsList.erase( this->_roomsList.find( userRoom->getId() ) );
	
		// then creating a new game
		Game* game = new Game(userRoom->getUsers(), userRoom->getQuestionsNo(), this->_db);
		if (game == nullptr)
			throw exception("Cannot Open Game");
		
		// starting the game
		game->sendFirstQuestion();
	}
	catch (...) {
		message << SERVER_START_GAME << 0;
		Helper::sendData(user->getSocket(), message.str());
	}

}
void TriviaServer::handlePlayerAnswer(ReceivedMessage* msg)
{
	User* user = msg->getUser();
	Game* userGame = user->getGame();
	
	if (userGame)
	{
		// sending the user, answer number, and time to answer, to the user's game to handle
		bool ret = userGame->handleAnswerFromUser(user, atoi(msg->getValues()[0].c_str()), atoi(msg->getValues()[1].c_str()));
	}
}
void TriviaServer::handleLeaveGame(ReceivedMessage* msg)
{
	Game* userGame = msg->getUser()->getGame();
	
	if (userGame != nullptr)
		if (!msg->getUser()->leaveGame())
			delete userGame;
}

// ROOM HANDLERS
bool TriviaServer::handleCloseRoom(ReceivedMessage* msg)
{
	bool ret = false;
	User* user = msg->getUser();
	Room* usrRoom = user->getRoom();
	
	if (usrRoom != nullptr && user->closeRoom() != -1)
	{
		// getting the room's place in the map. and erasing it
		for (map<int, Room*>::iterator it = this->_roomsList.begin(); it != this->_roomsList.end(); it++)
		{
			if (it->second == usrRoom)
			{
				this->_roomsList.erase(it);
				ret = true;
				
				break;
			}
		}
	}
	return ret;
}
bool TriviaServer::handleLeaveRoom(ReceivedMessage* msg)
{
	bool retVal = false;
	User* msgUser = msg->getUser();

	if (msgUser != nullptr)
	{
		Room* msgUserRoom = msgUser->getRoom();
		if (msgUserRoom != nullptr)
		{
			msgUser->leaveRoom();
			retVal = 0;
		}
	}

	return retVal;
}

bool TriviaServer::handleCreateRoom(ReceivedMessage* msg)
{
	bool retVal = false;
	User* user = msg->getUser();

	if (user != nullptr)
	{
		vector<string> values = msg->getValues();
		this->_roomIdSequence++;

		// creating the room with the given data from message
		if ( user->createRoom(this->_roomIdSequence, values[0], atoi(values[1].c_str()), atoi(values[2].c_str()), atoi(values[3].c_str())) )
			// and finally inserting the created room to the list of rooms. 
			this->_roomsList.insert(make_pair(user->getRoom()->getId(), user->getRoom()));

		retVal = true;
	}

	return retVal;
}
bool TriviaServer::handleJoinRoom(ReceivedMessage* msg)
{
	bool retVal = false;
	stringstream messageToSend;

	User* user = msg->getUser();

	if (user != nullptr)
	{
		messageToSend << SERVER_JOIN_ROOM;

		// getting the room's id from message
		int roomId = atoi(msg->getValues()[0].c_str());

		Room* room = this->getRoomById(roomId);
		if (room == nullptr)
		{
			messageToSend << EXCEPT_CODE;
		}
		else
		{	
			if (user->joinRoom(room))
				retVal = true;
		}
	}

	return retVal;
}
void TriviaServer::handleGetRooms(ReceivedMessage* msg)
{
	User* user = msg->getUser();
	stringstream message;

	message << SERVER_ROOMS_LIST;
		
	message << setfill('0') << setw(4) << this->_roomsList.size();

	if (this->_roomsList.size() > 0)
	{
		for (map<int, Room*>::iterator currRoom = this->_roomsList.begin(); currRoom != this->_roomsList.end(); currRoom++)
		{
			message << setfill('0') << setw(4) << currRoom->first; // id
			message << setfill('0') << setw(2) << currRoom->second->getName().length(); // length of name
			message << currRoom->second->getName(); // name
		}
	}

	Helper::sendData(user->getSocket(), message.str());
}
void TriviaServer::handleGetUsersInRoom(ReceivedMessage* msg)
{
	User* user = msg->getUser();
	int roomId = atoi(msg->getValues()[0].c_str());
	Room* room = getRoomById(roomId);

	stringstream message;
	
	if (room == nullptr)
		message << 0;
	else
		message << room->getUsersListMessage();

	Helper::sendData(user->getSocket(), message.str());
}

void TriviaServer::handleGetPersonalStatus(ReceivedMessage* msg)
{
	stringstream messageToSend;
	vector<string> messageData = this->_db.getPersonalStatus(msg->getUser()->getUsername());

	messageToSend << SERVER_PERSONAL_STATUS << messageData[0] << messageData[1] << messageData[2] << messageData[3];
	Helper::sendData(msg->getSock(), messageToSend.str());
}
void TriviaServer::handleGetBestScores(ReceivedMessage* msg)
{
	stringstream message;
	User* user = msg->getUser();
	vector<string> bestscore = this->_db.getBestScores();
	if (bestscore.size() == 6)//normal - 3 users and they score
	{
		message << SERVER_BEST_SCORES << setfill('0') << setw(2) << bestscore[0].length() << bestscore[0] << setfill('0') << setw(6) << bestscore[1];
		message << setfill('0') << setw(2) << bestscore[2].length() << bestscore[2] << setfill('0') << setw(6) << bestscore[3];
		message << setfill('0') << setw(2) << bestscore[4].length() << bestscore[4] << setfill('0') << setw(6) << bestscore[5];
	}
	else if (bestscore.size() == 4)//not normal - 2 users and they score
	{
		message << SERVER_BEST_SCORES << setfill('0') << setw(2) << bestscore[0].length() << bestscore[0] << setfill('0') << setw(6) << bestscore[1];
		message << setfill('0') << setw(2) << bestscore[2].length() << bestscore[2] << setfill('0') << setw(6) << bestscore[3];
		message << "00" << "000000" << "" ;
	}
	else if (bestscore.size() == 2)//not normal - 1 user and he score
	{
		message << SERVER_BEST_SCORES << setfill('0') << setw(2) << bestscore[0].length() << bestscore[0] << setfill('0') << setw(6) << bestscore[1];
		message << "00" << "000000" << "";
		message << "00" << "000000" << "";
	}
	else if (bestscore.size() == 0 && bestscore.empty() == true)//not players at all
	{
		message << SERVER_BEST_SCORES << "00" << "000000" << "";
		message << "00" << "000000" << "";
		message << "00" << "000000" << "";
	}
	msg->getUser()->send(message.str());
}

// HELP FUNCTIONS
Room* TriviaServer::getRoomById(int id)
{
	map<int, Room*>::iterator retRoom = this->_roomsList.find(id);

	// if found a room returns it, if not, returns 
	return retRoom == this->_roomsList.end() ? nullptr: retRoom->second;
}
User* TriviaServer::getUserByName(string username)
{
	User* foundUser = nullptr;
	
	// iterating over all users, and comparing their name to the given username.
	for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
		if ((*it).second->getUsername() == username)
			foundUser = (*it).second;

	return foundUser;
}
User* TriviaServer::getUserBySocket(SOCKET soc)
{
	User* foundUser = nullptr;

	// iterating over all users, and comparing their name to the given username.
	for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
		if ((*it).first == soc)
			foundUser = (*it).second;

	return foundUser;
}
void TriviaServer::safeDeleteUser(ReceivedMessage* msg)
{
	try 
	{
		SOCKET sc = msg->getSock();
		this->handleSignOut(msg);
		closesocket(sc);
		cout << "------------\nClient Number - " << sc << " Was Disconnected And Was Released From Memory\n------------" << endl;
	}
	catch (...) 
	{
		cout << "ERROR: closing client's socket failed" << endl;
	}
}
