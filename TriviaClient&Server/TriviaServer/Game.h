#ifndef GAME_H
#define GAME_H

#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iomanip>

#include "User.h"
#include "Question.h"
#include "DataBase.h"
#include "Protocol.h"

using namespace std;

class User;
class Question;

class Game{
public:
	Game(const vector <User*> & players, int queNo, DataBase& db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* u);
	int getID();

private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	DataBase* _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	int _currQuestionIndex;
	int _ID;

	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();

};

#endif