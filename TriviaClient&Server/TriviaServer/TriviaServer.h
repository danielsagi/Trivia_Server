#ifndef TRIVIA_SERVER
#define TRIVIA_SERVER

#include <WinSock2.h>
#include <Windows.h>
#include <mutex>
#include <queue>
#include <thread>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <exception>
#include <condition_variable>

#include "User.h"
#include "ReceivedMessage.h"
#include "Game.h"
#include "Room.h"
#include "Validator.h"
#include "Protocol.h"
#include "DataBase.h"

using namespace std;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();

	void serve();

private:
	SOCKET _socket;
	queue<ReceivedMessage*> _queRcvMessages;
	mutex _mtxReceivedMessages;
	condition_variable _rcvMessagesCond;
	map<SOCKET, User*> _connectedUsers;
	map<int, Room*> _roomsList;
	DataBase _db;

	static int _roomIdSequence;


	void bindAndListen();
	void accept();
	void clientHandler(SOCKET client);
	void handleReceivedMessages();
	void safeDeleteUser(ReceivedMessage*);

	User* handleSignin(ReceivedMessage*);
	bool  handleSignup(ReceivedMessage*);
	void  handleSignOut(ReceivedMessage*);

	void handleStartGame(ReceivedMessage*);
	void handleLeaveGame(ReceivedMessage*);
	void handlePlayerAnswer(ReceivedMessage*);

	bool handleCloseRoom(ReceivedMessage*);
	bool handleLeaveRoom(ReceivedMessage*);
	bool handleCreateRoom(ReceivedMessage*);
	bool handleJoinRoom(ReceivedMessage*);
	void handleGetRooms(ReceivedMessage*);
	void handleGetUsersInRoom(ReceivedMessage*);

	void handleGetBestScores(ReceivedMessage*);
	void handleGetPersonalStatus(ReceivedMessage*);

	ReceivedMessage* buildRecieveMessage(SOCKET, int);
	void addReceivedMessage(ReceivedMessage*);

	User* getUserBySocket(SOCKET);
	User* getUserByName(string);
	Room* getRoomById(int);

};

#endif