#include "Room.h"

Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime)
{
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = questionsNo;
	this->_questionsNo = questionsNo;
	this->_questionTime = questionTime;
	this->_users.push_back(admin);
}


Room::~Room()
{
}

User* Room::getAdmin()
{
	User* ret = NULL;
	if (this->_admin)
	{
		ret = this->_admin;
	}
	return ret;
}

int Room::getQuestionsNo()
{
	return this->_questionsNo;
}
int Room::getId()
{
	return this->_id;
}
string Room::getName()
{
	return this->_name;
}

/* 
joins a user to the room.
Returns TRUE if succeded
Returns FALSE if not
*/
bool Room::joinRoom(User* user)
{
	bool ret = false;
	
	stringstream replyMessage;
	replyMessage << SERVER_JOIN_ROOM;

	try
	{
		// if room is not full
		if (this->_users.size() < this->_maxUsers)
		{
			this->_users.push_back(user);
		
			// setting variables to success
			replyMessage << SUCCESS_CODE << setfill('0') << setw(2) << this->_questionsNo << setfill('0') << setw(2) << this->_questionTime;
			this->sendMessage(user, replyMessage.str());
					
			// getting the list of all users and sending to each user in the vector
			string messageToSend = this->getUsersListMessage();
			this->sendMessage(messageToSend);

			ret = true;
		}

		else
		{
			replyMessage << FAILED_CODE;
			this->sendMessage(user, replyMessage.str());
		}
	}
	catch (...)
	{
		replyMessage << EXCEPT_CODE;
		this->sendMessage(user, replyMessage.str());
	}
	
	// ?? find a better way to send the reply message, to save ugly lines of code
	return ret;
}

/* returns the formatted 108 message containing the list of users */
string Room::getUsersListMessage()
{
	stringstream completeMessage;

	// if room is not empty, constructing the message
	if (this->_users.size() != 0)
	{
		completeMessage << SERVER_ROOM_USERS << this->_users.size();

		for (unsigned int i = 0; i < this->_users.size(); i++)
		{
			string username = this->_users[i]->getUsername();
			completeMessage << setfill('0') << setw(2) << username.length() << username;
		}
	}
	else
	{
		// else, constructing an error message
		completeMessage << SERVER_ROOM_USERS << 0;
	}

	return completeMessage.str();
}

void Room::leaveRoom(User* user)
{
	stringstream message;

	vector<User*>::iterator it = find(this->_users.begin(), this->_users.end(), user);
	
	// if user was found in the connected users list.
	if (it != this->_users.end())
	{
		// sending him a confirmation that he left, and removing him from vector
		message << SERVER_LEAVE_ROOM << SUCCESS_CODE;
		this->sendMessage(*it, message.str());
		this->_users.erase(it);

		// updating all the users of the current state of connected users
		this->sendMessage(this->getUsersListMessage());
	}
}

int Room::closeRoom(User* user)
{
	int retVal = -1;
	stringstream message;

	// sends a close room message to all users if the user who requested the closure, 
	// is indeed the admin
	if (user == this->_admin)
	{
		message << SERVER_CLOSE_ROOM;
		this->sendMessage(message.str());
		retVal = this->_id;

		// iterating over all users, and unless they are the admin, clearing their room.
		for (vector<User*>::iterator it = this->_users.begin(); it < this->_users.end(); it++) 
			(*it)->clearRoom();
		
	}

	return retVal;
}

void Room::sendMessage(string message)
{
	for (unsigned int i = 0; i < this->_users.size(); i++)
		this->sendMessage(this->_users[i], message);
}

void Room::sendMessage(User* user, string message)
{
	Helper::sendData(user->getSocket(), message);
}

vector<User*> Room::getUsers()
{
	return this->_users;
}