#include "SqlTable.h"

sqlTable::sqlTable(string* IdName, int len)
{
	_IdName = IdName;
	_len = len;
	_Row = NULL;
}

sqlTable::~sqlTable()
{
}

string sqlTable::operator[](string IdName)
{
	for (int i = 0; i < _len; i++)
	{
		if (_IdName[i] == IdName)
		{
			return _Row[i];
		}
	}
	return NULL;
}

string sqlTable::operator[](int id)
{
	for (int i = 0; i < _len; i++)
	{
		if (i == id)
		{
			return _Row[i];
		}
	}
	return NULL;
}

void sqlTable::setRow(string* row)
{
	_Row = row;
}

int sqlTable::getLenRow()
{
	return _len;
}

string* sqlTable::getIdName()
{
	return _IdName;
}

string* sqlTable::getRow()
{
	return _Row;
}