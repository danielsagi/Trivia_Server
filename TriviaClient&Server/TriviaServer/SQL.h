#pragma once
#include <iostream>
#include <vector>
#include <string>

#include "sqlite3.h"
#include "SqlTable.h"

#define TEXT "text"
#define INTEGER "integer"

using namespace std;

class SQL
{
public:
	SQL(string);
	~SQL();

	// example use: createTable("users", new string[3]{"id", "name", "pass"}, new string[3]{"integer", "text", "text"}, 3, true);
	bool createTable(string NameTable, string* ParamsName, string* Types, int LengthParams, bool FirstIsPrimaryKey);
	// example use: existTable("users");
	bool existTable(string NameTable);
	// example use: insertToTable("users", new string[3]{"id", "name", "pass"}, new string[3]{"1", "eli", "123"}, 3);
	bool insertToTable(string NameTable, string* IdName, string* ParamsName, int LengthParams);
	// example use: updateRowInTable("users", id=4, name='yosi');  
	bool updateRowInTable(string NameTable, string id, string updateCMD);
	// example use: removeRowInTable("users", id=4, false);
	bool removeRowInTable(string NameTable, string id, bool removeAll);
	// example use: deleteTable("users");
	bool deleteTable(string NameTable);
	// example use: renameTable("users","students");
	bool renameTable(string NameTable, string newName);
	// example use: addColumnToTable("users","age","integer");
	bool addColumnToTable(string NameTable, string nameCol, string type);
	/* example use:
	*  vector<sqlTable> allUsers = getFromTable("users",""); || getFromTable("users","where id=1");
	*  
	*  cout << allUsers[0]["id"] << endl;
	*  cout << allUsers[0]["pass"] << endl;
	* */
	vector<sqlTable> getFromTable(string NameTable, string addCMD);


	static bool isText(string text);
private:
	int _rc;
	sqlite3* _db;
	char* _zErrMsg = 0;
	bool _flag = true;

	void clearVector();
	bool ERRORS();
};
