#pragma once

#include "User.h"

using namespace std;

User::User(string username, SOCKET sock)
{
	this->_username = username;
	_sock = sock; 
	_currGame = nullptr;
	_currRoom = nullptr;

}

User::~User()
{
	if (this->_currGame)
		delete this->_currGame;
	if (this->_currRoom)
		delete this->_currRoom;
}

void User::send(string message)
{
	Helper::sendData(this->_sock, message);
}

string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room* User::getRoom()
{
	return _currRoom;
}

Game* User::getGame()
{
	return _currGame;
}

void User::setGame(Game* game)
{
	_currRoom = nullptr;
	_currGame = game; 
}

void User::clearRoom()
{
	_currRoom = nullptr;
}
void User::clearGame()
{
	_currGame = nullptr;
}
bool User::createRoom(int id, string name, int maxUsers, int questionNo, int questionTime)
{
	if (_currRoom == nullptr)//if null make new
	{
		Room* room = new Room(id, this, name, maxUsers, questionNo, questionTime);
		_currRoom = room;
		send("1140");//sucsess
		return true; 
	}
	else//if no null so send error
	{
		send("1141");//fail
		return false;
	}
}

bool User::joinRoom(Room* room){

	if (_currRoom != nullptr)
	{
		return false;
	}
	else{
		if (room->joinRoom(this))
			this->_currRoom = room;
		return true;
	}
}

void User::leaveRoom(){
	if (_currRoom){
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom(){
	int retVal = _currRoom->closeRoom(this);

	if (retVal != -1)
	{
		delete _currRoom;
		_currRoom = nullptr;
	}
	
	return retVal;
}

bool User::leaveGame(){
	bool ret = false; 

	if (this->_currGame)
	{
		if (_currGame->leaveGame(this))
			ret = true;

		_currGame = nullptr;
	}
	return ret;
}