#include "Validator.h"


// ?? add regex
bool Validator::isPasswordValid(string password)
{
	int countU = 0, countL = 0, countD = 0, countS = 0;

	for (int i = 0; i < password.length(); i++)
	{
		if (isupper(password[i]))
			countU++;
		else if (islower(password[i]))
			countL++;
		else if (isdigit(password[i]))
			countD++;
		else if (isspace(password[i]))
			countS++;
	}

	return (countU + countL + countD + countS) >= 4 && password.length() >= 4 && password.find(" ") == -1;
}

bool Validator::isUsernameValid(string username)
{
	bool ret = false;
	
	// not empty, first is alpha, doesnt contain space
	if (username.length() > 0 && isalpha(username[0]) && username.find(" ") == string::npos) 
		ret = true;
	
	return ret;
}


