﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class StartGameOrWaitTo : Form
    {

        Server _server;
        Form _thefirstform;
        string _nameRoom;
        string _IdRoom;
        List<string> _names;

        public StartGameOrWaitTo(Server server , Form thefirstform , string nameRoom , string IdRoom , List<string> names )
        {
            status.ForeColor = System.Drawing.Color.Red;
            status.Text = "UnConnect";
            _server = server;
             _thefirstform = thefirstform;
            _nameRoom = nameRoom;
            _IdRoom = IdRoom;
            _names = names;

            InitializeComponent();
            label1.Text += nameRoom; 
            
            for (int i = 0; i < _names.Count ; i++)
            {
                Users.Items.Add(_names[i]);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Leave game - go to menu 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Threading.Thread joinusers;
            joinusers = new System.Threading.Thread(JoinUsers);
            joinusers.Start();
        }

        public void JoinUsers()
        {
            _server.send("209" + _IdRoom);

            string recv = _server.recv(1024);


        }
    }
}
