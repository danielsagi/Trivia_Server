﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class Form1 : Form
    {

        public string _ip = "127.0.0.1";
        public string _port = "27493";
        public bool _encrypt = true;
        public Server _server = new Server();
        


        public Form1()
        {
            InitializeComponent();
            _server.connect(_ip, Int32.Parse(_port));

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
          
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form settings = new Settings();
            settings.Show();
            
            
                 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form login = new LogIn(_encrypt,_server,this);
            login.Show();
            
            //show login window 
            //if good login hide the first form and when he make sigout we make the first from to show 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form signup = new SignUp(_encrypt, _server,this);
            signup.Show();
        }

        

      
      
    }
}
