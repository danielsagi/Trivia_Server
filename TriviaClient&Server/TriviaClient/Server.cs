﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace TriviaClient
{
    public class Server
    {


        //server = connect , send , recv , close . 
        public byte[] _bufferout;
        public byte[] _bufferIN;

        NetworkStream clientstraem = null;
        TcpClient client = new TcpClient();

        public void connect(string ip , int port )
        {
            client = new TcpClient();
            IPEndPoint ServerEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            try
            {
                client.Connect(ServerEndPoint);
            }
            catch
            {
                MessageBox.Show("server close.", "ERROR");
                Environment.Exit(0);
            }

            clientstraem = client.GetStream();
        }

        public void send(string send)
        {
            _bufferout = new ASCIIEncoding().GetBytes(send);
            clientstraem.Write(_bufferout, 0, _bufferout.Length);
            clientstraem.Flush();

        }

        public string recv(int len)
        {
            _bufferIN = new byte[len];
            int bufferread = clientstraem.Read(_bufferIN, 0, len);
            string recv = new ASCIIEncoding().GetString(_bufferIN);
            return recv;
        }

        public void close()
        {
            clientstraem.Close();
            client.Close();
        }



    }
}
