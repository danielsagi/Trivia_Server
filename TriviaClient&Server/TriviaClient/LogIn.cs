﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class LogIn : Form
    {

        public const string OK = "1020";
        public static bool _encrypt;
        Server _server;
        public Form _theFirstForm;

        public LogIn(bool encrypt,Server server ,Form theFirst)
        {
            InitializeComponent();
            password.PasswordChar = '*';

            _encrypt = encrypt;
            _server = server;
            _theFirstForm = theFirst; 

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Form signup = new SignUp(_encrypt,_server,_theFirstForm);
            signup.Show();
            this.Close();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            System.Threading.Thread logInThread;
            logInThread = new System.Threading.Thread(loginThread);
            logInThread.Start();
        }

        public void loginThread()
        {
            string Username = username.Text;
            string Pass = password.Text;

            _server.send("200" + Username.Length.ToString().PadLeft(2, '0') + Username + Pass.Length.ToString().PadLeft(2, '0') + Pass);

            string recv = _server.recv(4);

            if (recv == OK)
            {
                Invoke((MethodInvoker)delegate {
                    loginmsg.Text = "You've logged in successfully";
                    loginmsg.Refresh();
                    System.Threading.Thread.Sleep(500);
                    Form2 form2 = new Form2(_server, Username,_theFirstForm);
                    form2.Show();
                    _theFirstForm.Hide();
                    this.Close();

                });

            }
            else if (recv == "1021")
            {
                Invoke((MethodInvoker)delegate { loginmsg.Text = "Wrong Details"; });
            }
            else if (recv == "1022")
            {
                Invoke((MethodInvoker)delegate {
                    loginmsg.Text = "User is already connected";
                });

            }

        }


    }
}
