﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class JoinRoom : Form
    {

        public List<string> _ListRooms;
        public Server _server;
        public Form _thefirstform;
        List<string> _ListNames = new List<string>();
        string _nameOfRoom;
        string _idOfRoom;


        public JoinRoom(List<string> ListRooms , Server server , Form thefirstform)
        {
            _thefirstform = thefirstform;
            _server = server;
            _ListRooms = ListRooms; 

            InitializeComponent();

            for (int i = 1 ; i < ((_ListRooms.Count)); i=i+2)
            {
                Rooms.Items.Add(ListRooms[i]);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string selectedRoomName = Rooms.SelectedItem.ToString();
            int index = _ListRooms.IndexOf(selectedRoomName);
            string IdSelectedRoom = _ListRooms[index - 1];
            _nameOfRoom = selectedRoomName;
            _idOfRoom = IdSelectedRoom;


            System.Threading.Thread getnames;
            getnames = new System.Threading.Thread(getNamesThread);
            getnames.Start();


        }

        public void getNamesThread()
        {
            _server.send("207" + _idOfRoom);
            string recv = _server.recv(1024);

            if (recv.Substring(0,3) == "108" && recv.Substring(0,4) != "1080")
            {
                string Justnames = recv.Substring(3);
                int NumOfUsers = Int32.Parse(Justnames[0].ToString());
                Justnames = Justnames.Substring(1);
                for (int i = 0; i<NumOfUsers; i++)
                {
                    int NumOfLettersInName = Int32.Parse(Justnames.Substring(0, 2));
                    Justnames = Justnames.Substring(2);
                    string name = Justnames.Substring(0, NumOfLettersInName);
                    _ListNames.Add(name);
                    Justnames = Justnames.Substring(NumOfLettersInName);
                }

                Invoke((MethodInvoker)delegate
                {
                    StartGameOrWaitTo game = new StartGameOrWaitTo(_server, _thefirstform, _nameOfRoom, _idOfRoom, _ListNames);
                    game.Show();
                });

            }
            else if (recv.Substring(0,4) == "1080")
            {
                _ListNames.Add("NoOne");
                Invoke((MethodInvoker)delegate
                {
                    StartGameOrWaitTo game = new StartGameOrWaitTo(_server, _thefirstform, _nameOfRoom, _idOfRoom, _ListNames);
                    game.Show();
                });
            }
            else
            {
                //Todo - ERROR ; 
            }


        }
    }
}
