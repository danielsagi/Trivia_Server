﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class Form2 : Form
    {
        public const int MAX_SIZE = 1024;
        public Server _server;
        public string _username;
        public Form _theFirstForm;

        public Form2(Server server , string username , Form theFirstForm)
        {
            InitializeComponent();
            _server = server;
            _username = username;
            label1.Text += username; 
           // _theFirstForm = theFirstForm;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Threading.Thread SIGNoutThread;
            SIGNoutThread = new System.Threading.Thread(signoutrthread);
            SIGNoutThread.Start();
        }

        void signoutrthread()
        {
            _server.send("299");
            Invoke((MethodInvoker) delegate{ Environment.Exit(0); });
        }

        private void button5_Click(object sender, EventArgs e)
        {

            _server.send("223");

            string recv = _server.recv(99);
            string numOfLetters;
            string temp;
            string best1;
            string best2;
            string best3;
            if (recv[0].ToString() + recv[1].ToString() + recv[2].ToString() == "124" && recv.Length > 4)
            {
                numOfLetters = recv[3].ToString() + recv[4].ToString();
                best1 = recv.Substring(5,6+Int32.Parse(numOfLetters));
                numOfLetters = recv[11+ Int32.Parse(numOfLetters)].ToString() + recv[12 + Int32.Parse(numOfLetters)].ToString();
                best2 = recv.Substring(11 + Int32.Parse(numOfLetters), 6 + Int32.Parse(numOfLetters));
                numOfLetters = recv[20 + Int32.Parse(numOfLetters)].ToString() + recv[21 + Int32.Parse(numOfLetters)].ToString();
                best3 = recv.Substring(16 + Int32.Parse(numOfLetters), 11 + Int32.Parse(numOfLetters));

            }
        }

        public void bestscorethread()
        {
            _server.send("233");
            string recv = _server.recv(99);
            string num;
            string best1;
            string best2;
            string best3;
            if (recv[0].ToString() + recv[1].ToString() + recv[2].ToString() == "124")
            {
                num = recv[3].ToString() + recv[4].ToString();
                best1 = recv.Substring(5,5+Int32.Parse(num));

            }

            Invoke((MethodInvoker)delegate {});
        }

        private void button3_Click(object sender, EventArgs e)
        {

            System.Threading.Thread joinroomthread;
            joinroomthread = new System.Threading.Thread(JoinRoomThread);
            joinroomthread.Start();

        }

        public void JoinRoomThread()
        {
            _server.send("205");

            string recv = _server.recv(MAX_SIZE);
            int numOfRooms;

            if (recv.Substring(0, 3) == "106")
            {
                numOfRooms = Int32.Parse(recv.Substring(3, 4));

                if (numOfRooms > 0)
                {
                    List<string> list = new List<string>();

                    int LenOfName;

                    string JustRooms = recv.Substring(7);

                    for (int i = 0; i < numOfRooms; i++)
                    {
                        list.Add(JustRooms.Substring(0, 4));
                        JustRooms = JustRooms.Substring(4);
                        LenOfName = Int32.Parse(JustRooms.Substring(0, 2));
                        JustRooms = JustRooms.Substring(2);
                        list.Add(JustRooms.Substring(0, LenOfName));
                        JustRooms = JustRooms.Substring(LenOfName);
                    }
                    Invoke((MethodInvoker)delegate {
                        JoinRoom form3 = new JoinRoom(list, _server, _theFirstForm);
                        form3.Show(); ;
                    });
                    
                }
                else
                {
                    //TODO : ERROR / - THERE IS NO ROOMS.
                }

            }
            else
            {
                //TODO : SEND ERROR .
            }
        }
    }
}
