﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class SignUp : Form
    {

        Server _server;
        public static bool _encrypt;
        public Form _theFirstForm;

        public SignUp(bool encrypt,Server server, Form theFirst)
        {
            InitializeComponent();
            _server = server; 
            _encrypt = encrypt;
            _theFirstForm = theFirst;
            password.PasswordChar = '*';


        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Form login = new LogIn( _encrypt,_server,_theFirstForm);
            login.Show();
            this.Close();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {

            System.Threading.Thread SIGNUPThread;
            SIGNUPThread = new System.Threading.Thread(SignupThread);
            SIGNUPThread.Start();
          
        }

        public void SignupThread()
        {
            string Username = username.Text;
            string Pass = password.Text;
            string Mail = mail.Text;

            _server.send("203" + Username.Length.ToString().PadLeft(2, '0') + Username + Pass.Length.ToString().PadLeft(2, '0') + Pass + Mail.Length.ToString().PadLeft(2, '0') + Mail);

            string recv = _server.recv(4);

            if (recv == "1040")
            {
                Invoke((MethodInvoker)delegate {
                    sigupmsg.Text = "You've sigu-up successfully , please Login .";
                });
            }
            else if (recv == "1041")
            {
                Invoke((MethodInvoker)delegate {
                    sigupmsg.Text = "pass illegal";

                });
            }
            else if (recv == "1042")
            {
                Invoke((MethodInvoker)delegate {
                    sigupmsg.Text = "user name already exists";

                });
            }
            else if (recv == "1043")
            {
                Invoke((MethodInvoker)delegate {
                    sigupmsg.Text = "Invalid username";

                });
            }
            else if (recv == "1044")
            {
                Invoke((MethodInvoker)delegate {
                    sigupmsg.Text = "ERROR , Something happened";

                });
            }
        }

       
    }
}
